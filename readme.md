#Assesment - Orders - Jacobs
@(Alfasoft)[webdev, laravel]

![Jacobs|left](https://i.imgur.com/7Ono1o5.jpg)

[![PHP](https://img.shields.io/badge/PHP-v7.2-blue.svg)](http://php.net/)
[![Laravel](https://img.shields.io/badge/Laravel-v5.7-red.svg)](https://laravel.com/)
[![Horizon](https://img.shields.io/badge/Queue-Horizon-yellowgreen.svg)](https://horizon.laravel.com/)
[![Redis](https://img.shields.io/badge/Jobs-Redis-orange.svg)](https://redis.io/)
![Laravel|right](https://laravel.com/assets/img/components/logo-laravel.svg)

## Development deploy

### 1. Get submodule Laradock
1. Clone laradock on your project root directory:`git clone https://github.com/Laradock/laradock.git`
2. Copy env-example to .env executing:`cp env-example .env`

#### Only to windows
>**Adjust .Env ** - Create a folder and fix path mysql to windows: `DATA_SAVE_PATH=C:\temp\Jacobs\laradock\data`

### 3. Create a database
1. Copy `createdb.example.sql` to `createdb.sql`
	- path: `.\laradock\mysql\docker-entrypoint-initdb.d\createdb.sql`
2. Set table and user creation:
```
CREATE DATABASE IF NOT EXISTS `jacobs_db_assesment` COLLATE 'utf8_general_ci' ;
GRANT ALL ON `jacobs_db_assesment`.* TO 'default'@'%' ;

FLUSH PRIVILEGES ;
```

### 4. Config Laravel .env
1. Install packages `composer install`
2. If not exists, copy .`env.example` to create  `.env` on project laravel
3. Change call to containers name
	1. DB_HOST=`127.0.0.1` to `mysql`
	2. REDIS_HOST=`127.0.0.1`  to `redis`
	4. QUEUE_DRIVER=`sync` to `redis` (important to Horizon show jobs on dashboard)
4. Install packages node `yarn` or `npm install`
	- And create laravel assets `yarn run dev` or `npm run dev`
```
DB_HOST=mysql
# DB_HOST=127.0.0.1

REDIS_HOST=redis
# REDIS_HOST=127.0.0.1

QUEUE_DRIVER=redis
# QUEUE_DRIVER=sync
```

### 5. Run your containers
1. Inside the folder `{project}\laradock`
2. Up simple containers 
`docker-compose up -d nginx mysql redis`

> **MySql Table** 
> Instructions to solve when table was not created: 
> 1. enter on mySql container `docker-compose exec mysql bash`
> 2. execute creation manually:
>      - `mysql -u root -p < /docker-entrypoint-initdb.d/createdb.sql`
>      - password is `root`


### 6. Do deploy inside workspace
1. Open a bash: `docker-compose exec workspace bash`
2. It's always good to do `composer update`
3. Create every tables `php artisan migrate`
4. Install Passport `php artisan passport:install` and save on secure place Encryption keys
5. Seed user table with `php artisan db:seed`
6. Start Horizon `php artisan horizon`
7. Test application with `phpunit` or `./vendor/bin/phpunit`

![UnitTest](https://i.imgur.com/OdZGwJW.jpg)

---
## Api
After start Horizon `php artisan horizon`,  you can send posts and gets to endpoints.
### Header 
Here is the default header required on all REST calls to the API.
#### Content
``` 
Content-Type:application/json
Accept:application/json
```
#### Authentication
1. After that first user was created and on first login, the column `access_token` is filled on users table on database.
2. Obtaining this token from the admin user in the database requires inserting the token in the request header as follows:
``` 
Authorization:Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIm...
```

---
## Sequence diagrams
### Orders
#### 1. orders create
- Endpoint:  `api/ordersUpdate`
```sequence2
Origin->>OrdersController: `Post` update(json) 
Note right of Origin: Param : orders
OrdersController->>OrdersController: createUpdateOrders()
OrdersController->>OrderCreateUpdate: dispatch($orders)
```
> - [Source diagram](https://goo.gl/Y3phLU)
> - [View diagram](https://goo.gl/ynXSTX)

##### Json params
```json
{
	"orders":
	[
   {
      "name":"Mrs. Leonora Harvey II",
      "email":"erenner@example.com",
      "date":"1975-09-28 01:21:41",
      "sku":27531,
      "updated_at":"2019-01-26 19:04:14",
      "created_at":"2019-01-26 19:04:14",
      "id":51
   },
   {
      "name":"Steve Schaefer",
      "email":"reggie99@example.org",
      "date":"2011-09-20 10:32:19",
      "sku":52221,
      "updated_at":"2019-01-26 19:04:14",
      "created_at":"2019-01-26 19:04:14",
      "id":52
   },
   {
      "name":"Dr. Darien Anderson I",
      "email":"carlie70@example.net",
      "date":"2000-05-01 05:53:24",
      "sku":78097,
      "updated_at":"2019-01-26 19:04:14",
      "created_at":"2019-01-26 19:04:14",
      "id":53
   },
   {
      "name":"Brandyn Rutherford",
      "email":"kbeer@example.org",
      "date":"1998-07-13 04:11:51",
      "sku":54453,
      "updated_at":"2019-01-26 19:04:15",
      "created_at":"2019-01-26 19:04:15",
      "id":54
   },
   {
      "name":"Prof. Charlie Pollich",
      "email":"parker.ally@example.org",
      "date":"1975-11-03 16:41:54",
      "sku":91313,
      "updated_at":"2019-01-26 19:04:15",
      "created_at":"2019-01-26 19:04:15",
      "id":55
   },
   {
      "name":"Torrance Morar",
      "email":"lkrajcik@example.net",
      "date":"2018-12-14 12:44:46",
      "sku":86481,
      "updated_at":"2019-01-26 19:04:15",
      "created_at":"2019-01-26 19:04:15",
      "id":56
   },
   {
      "name":"Ms. Adriana King",
      "email":"dewitt.wunsch@example.net",
      "date":"1985-09-23 13:41:07",
      "sku":79077,
      "updated_at":"2019-01-26 19:04:15",
      "created_at":"2019-01-26 19:04:15",
      "id":57
   },
   {
      "name":"Mrs. Raina Steuber",
      "email":"charlie84@example.net",
      "date":"2011-03-16 22:27:31",
      "sku":58145,
      "updated_at":"2019-01-26 19:04:15",
      "created_at":"2019-01-26 19:04:15",
      "id":58
   },
   {
      "name":"Declan Feest",
      "email":"hschulist@example.com",
      "date":"1990-12-24 04:11:30",
      "sku":93524,
      "updated_at":"2019-01-26 19:04:15",
      "created_at":"2019-01-26 19:04:15",
      "id":59
   },
   {
      "name":"Darrell Orn",
      "email":"anderson.kiera@example.org",
      "date":"2015-03-02 11:26:31",
      "sku":56582,
      "updated_at":"2019-01-26 19:04:15",
      "created_at":"2019-01-26 19:04:15",
      "id":60
   }
]
}
```
You can see the queue execution through Horizon access `/horizon`:
![Horizon](https://i.imgur.com/5bODerT.jpg)



#### 2. orders get
##### a. All
- Endpoints:  
	- `api/getAll`
	- `api/getAll/failed`
	- `api/getAll/pending`
	- `api/getAll/finished`

```sequence2
Origin->>OrdersController: `getAll`
Origin->>OrdersController: `getAll` getAll/failed
Origin->>OrdersController: `getAll` getAll/pending
Origin->>OrdersController: `getAll` getAll/finished
Note right of OrdersController: Return: json
```
> - [Source diagram](https://goo.gl/XFGJwV)
> - [View diagram](https://goo.gl/PZjHmz)


See status (queued, executing or finished) of all execution like the result json:
```
{
    "job_statuses": [
        {
            "id": 1,
            "job_id": "58",
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": "default",
            "attempts": 1,
            "progress_now": 19,
            "progress_max": 19,
            "status": "finished",
            "input": null,
            "output": {
                "total": 19,
                "other": "parameter"
            },
            "created_at": "2019-01-27 14:51:33",
            "updated_at": "2019-01-27 14:51:57",
            "started_at": "2019-01-27 14:51:35",
            "finished_at": "2019-01-27 14:51:57"
        },
        {
            "id": 2,
            "job_id": "59",
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": "default",
            "attempts": 1,
            "progress_now": 28,
            "progress_max": 28,
            "status": "finished",
            "input": null,
            "output": {
                "total": 28,
                "other": "parameter"
            },
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:52:07",
            "started_at": "2019-01-27 14:51:35",
            "finished_at": "2019-01-27 14:52:07"
        },
        {
            "id": 3,
            "job_id": "60",
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": "default",
            "attempts": 1,
            "progress_now": 26,
            "progress_max": 26,
            "status": "finished",
            "input": null,
            "output": {
                "total": 26,
                "other": "parameter"
            },
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:52:05",
            "started_at": "2019-01-27 14:51:35",
            "finished_at": "2019-01-27 14:52:05"
        },
        {
            "id": 4,
            "job_id": "61",
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": "default",
            "attempts": 1,
            "progress_now": 23,
            "progress_max": 26,
            "status": "executing",
            "input": null,
            "output": null,
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:52:23",
            "started_at": "2019-01-27 14:51:57",
            "finished_at": null
        },
        {
            "id": 5,
            "job_id": "62",
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": "default",
            "attempts": 1,
            "progress_now": 15,
            "progress_max": 15,
            "status": "finished",
            "input": null,
            "output": {
                "total": 15,
                "other": "parameter"
            },
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:52:22",
            "started_at": "2019-01-27 14:52:05",
            "finished_at": "2019-01-27 14:52:22"
        },
        {
            "id": 6,
            "job_id": "63",
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": "default",
            "attempts": 1,
            "progress_now": 14,
            "progress_max": 21,
            "status": "executing",
            "input": null,
            "output": null,
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:52:23",
            "started_at": "2019-01-27 14:52:07",
            "finished_at": null
        },
        {
            "id": 7,
            "job_id": "64",
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": "default",
            "attempts": 1,
            "progress_now": 0,
            "progress_max": 12,
            "status": "executing",
            "input": null,
            "output": null,
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:52:23",
            "started_at": "2019-01-27 14:52:23",
            "finished_at": null
        },
        {
            "id": 8,
            "job_id": null,
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": null,
            "attempts": 0,
            "progress_now": 0,
            "progress_max": 0,
            "status": "queued",
            "input": null,
            "output": null,
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:51:34",
            "started_at": null,
            "finished_at": null
        },
        {
            "id": 9,
            "job_id": null,
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": null,
            "attempts": 0,
            "progress_now": 0,
            "progress_max": 0,
            "status": "queued",
            "input": null,
            "output": null,
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:51:34",
            "started_at": null,
            "finished_at": null
        },
        {
            "id": 10,
            "job_id": null,
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": null,
            "attempts": 0,
            "progress_now": 0,
            "progress_max": 0,
            "status": "queued",
            "input": null,
            "output": null,
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:51:34",
            "started_at": null,
            "finished_at": null
        }
    ]
}
```

See only with status `failed` execute endpoint `api/getAll/failed`:
```
{
    "job_statuses": [
        {
            "id": 9,
            "job_id": "66",
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": "default",
            "attempts": 1,
            "progress_now": 26,
            "progress_max": 26,
            "status": "failed",
            "input": null,
            "output": {
                "total": 26,
                "other": "parameter"
            },
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:53:00",
            "started_at": "2019-01-27 14:52:31",
            "finished_at": "2019-01-27 14:53:00"
        },
        {
            "id": 10,
            "job_id": "67",
            "type": "App\\Jobs\\OrderCreateUpdate",
            "queue": "default",
            "attempts": 1,
            "progress_now": 20,
            "progress_max": 20,
            "status": "failed",
            "input": null,
            "output": {
                "total": 20,
                "other": "parameter"
            },
            "created_at": "2019-01-27 14:51:34",
            "updated_at": "2019-01-27 14:53:00",
            "started_at": "2019-01-27 14:52:37",
            "finished_at": "2019-01-27 14:53:00"
        }
    ]
}
```


---
![Jacobs|center](https://i.imgur.com/7Ono1o5.jpg)
