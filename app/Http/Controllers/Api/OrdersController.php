<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Jobs\OrderCreateUpdate;
use App\Http\Controllers\Controller;
use Imtigger\LaravelJobStatus\JobStatus;

class OrdersController extends Controller
{
    /**
     * Create or Update Order
     * 
     * @param $request Json
     * @return json
     */
    public function update(Request $request)
    {

        $all = $request->all();

        if(isset($all['orders']))
        {
            if(!isset($all['test'])){ 
                
                $this->createUpdateOrders($all['orders']);

            }

            return response()->json([ 'message' => 'Orders updated!', 'count' => count($all['orders']) ]);

        }else{

            return response()->json([ 'message' => 'There is no order!', 'count' => 0 ]);

        }

    }


    /**
     * Create or update order
     * 
     * @return void
     */
    private function createUpdateOrders(array $orders){

        foreach($orders as $order){
            
            //OrderCreateUpdate::dispatch($order)->onQueue('default');

            $job = new OrderCreateUpdate($order);
            $this->dispatch($job);

            $jobStatusId = $job->getJobStatusId();
            // $jobStatus = JobStatus::find($jobStatusId);
            // dd($jobStatus);
        }

    }

    /**
     * Return all jobs statuses with or without filter
     *
     * @return json
     */
    public function getAll($filter = null){

        if(empty($filter)){

            $allJobStatus = JobStatus::all();

        }else if($filter == 'failed' or $filter == 'pending' or $filter == 'finished'){

            $allJobStatus = JobStatus::whereStatus($filter)->get();

        }else{

            return response()->json([ 'message' => 'Filter invalid!', 'count' => 0 ]);

        }

        return response()->json([ 'job_statuses' => $allJobStatus ]);

    }
}
