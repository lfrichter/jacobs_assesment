<?php

namespace App\Jobs;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Imtigger\LaravelJobStatus\Trackable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class OrderCreateUpdate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Trackable;

    protected $order;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($order)
    {
        $this->prepareStatus();
        $this->order = $order;
    }

    /**
     * Get the tags that should be assigned to the job.
     *
     * @return array
     */
    public function tags()
    {
        return ['order', 'OrderCreateUpdate'];
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // ------------------| Calc
        // $max = mt_rand(5, 30);
        // $this->setProgressMax($max);

        // for ($i = 0; $i <= $max; $i += 1) {
        //     sleep(1); // Some Long Operations
        //     $this->setProgressNow($i);
        // }

        // $this->setOutput(['total' => $max, 'other' => 'parameter']);

        $order = Order::firstOrNew(['sku' => $this->order['sku']]);
        $order->fill($this->order);
        $order->save();

    }
}
