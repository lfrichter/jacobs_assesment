<?php

namespace Tests\Unit;

use App\User;
use App\Order;
use Tests\TestCase;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderTest extends TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    private function getUser(){

        $arr = ['name' => 'Dev', 'email' => 'dev@dev.com'];
        $user = User::where($arr)->first();

        if(empty($user)){
            Artisan::call('passport:install');
            $user = User::create(array_merge($arr, ['password' => Hash::make('123456')]));
            $user->access_token = $user->createToken('P4G3D6')->accessToken;
            $user->update();
        }

        return $user;
    }

    private function getHeader(){

        $token = $this->getUser()->access_token;

        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '. $token
        ];
    }

    public function test_create_order_without_authentication() {

        $orders = ['orders' => factory(Order::class, 1)->create()->toArray()];

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'])
        ->json('POST', route('orders.update'), $orders);

        $response->assertStatus(401)->assertJson([
            "message" => "Unauthenticated."
        ]);
        
    }

    public function test_getAll_orders_without_authentication() {

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'])
        ->json('GET', route('orders.getAll'));

        $response->assertStatus(401)->assertJson([
            "message" => "Unauthenticated."
        ]);
        
    }

    public function test_can_create_orders() {

        $orders = ['orders' => factory(Order::class, 3)->create()->toArray()];

        $response = $this->withHeaders($this->getHeader())
        ->json('POST', route('orders.update'), $orders);

        $response->assertStatus(200)->assertJson([
            "message" => "Orders updated!","count" => 3
        ]);
        
    }

    public function test_can_create_empty_order() {

        $orders = [];

        $response = $this->withHeaders($this->getHeader())
        ->json('POST', route('orders.update'), $orders);

        $response->assertStatus(200)->assertJson([
            "message" => "There is no order!","count" => 0
        ]);
        
    }

    public function test_can_getAll() {

        $response = $this->withHeaders($this->getHeader())
        ->json('GET', route('orders.getAll'));

        $response->assertStatus(200);
    }

    public function test_can_getAll_finished() {

        $response = $this->withHeaders($this->getHeader())
        ->json('GET', route('orders.getAll').'/finished');

        $response->assertStatus(200);
    }

    public function test_can_getAll_pending() {

        $response = $this->withHeaders($this->getHeader())
        ->json('GET', route('orders.getAll').'/pending');

        $response->assertStatus(200);
    }

    public function test_can_getAll_failed() {

        $response = $this->withHeaders($this->getHeader())
        ->json('GET', route('orders.getAll').'/failed');

        $response->assertStatus(200);
    }

    public function test_can_getAll_finished_without_authentication() {

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'])
        ->json('GET', route('orders.getAll').'/finished');

        $response->assertStatus(401)->assertJson([
            "message" => "Unauthenticated."
        ]);
    }

    public function test_can_getAll_pending_without_authentication() {

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'])
        ->json('GET', route('orders.getAll').'/pending');

        $response->assertStatus(401)->assertJson([
            "message" => "Unauthenticated."
        ]);
    }

    public function test_can_getAll_failed_without_authentication() {

        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'])
        ->json('GET', route('orders.getAll').'/failed');

        $response->assertStatus(401)->assertJson([
            "message" => "Unauthenticated."
        ]);
    }
}
