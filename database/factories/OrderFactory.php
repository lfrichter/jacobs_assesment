<?php

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'date' => $faker->dateTime($max = 'now', $timezone = null)->format('Y-m-d H:i:s'),
        'sku' => rand ( 10000 , 99999 )
    ];
});
