<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CreateUserDev extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = ['name' => 'Dev', 'email' => 'dev@dev.com'];
        $user = User::where($arr)->first();

        if(empty($user)){
            $user = User::create(array_merge($arr, ['password' => Hash::make('123456')]));
            $user->access_token = $user->createToken('P4G3D6')->accessToken;
            $user->update();
        }
        
        return $user;
    }
}
